﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace DialogUnlockNow
{
	public class UIImagePreview : MonoBehaviour, IPointerDownHandler, ISlideable, IZoomable
	{
		const float animationSpeed = 0.5f;

		Vector2 _startingAnchor;
		readonly Vector2 _zoomedInAnchor = new Vector2 (0f, 0f);
		Vector2 _startingScale;
		readonly Vector2 _zoomedInScale = new Vector2 (1f, 1f);
		RectTransform _rectTransform;

		bool IZoomable.IsZoomed { get; set; }

		[SerializeField]
		int _index;

		int ISlideable.Index { 
			get {
				return _index;
			}
			set {
				_index = value;
			}
		}


		event Action<IZoomable> Zoom;

		event Action<IZoomable> IZoomable.Zoom {
			add {
				Zoom += value;
			}
			remove {
				Zoom -= value;
			}
		}

		void OnZoom (IZoomable sender)
		{
			if (Zoom != null)
				Zoom (sender);
		}



		event Action<ISlideable> Focus;

		event Action<ISlideable> ISlideable.Focus {
			add {
				Focus += value;
			}
			remove {
				Focus -= value;
			}
		}

		void OnFocus (ISlideable sender)
		{
			if (Focus != null)
				Focus (sender);
		}



		void IZoomable.ZoomIn ()
		{
			ISlideable slideable = this as ISlideable;

			if (slideable.IsZoomed)
				return;

			OnZoom (slideable as IZoomable);

			AddCanvas ();
			MoveTo (_zoomedInAnchor);
			ScaleTo (
				(scalingFinished) => {
					if (scalingFinished) {
						slideable.IsZoomed = true;
						OnFocus (slideable);
					}
				},
				_zoomedInScale
			);
		}



		void AddCanvas ()
		{
			Canvas _canvas = gameObject.AddComponent<Canvas> ();
			_canvas.overrideSorting = true;
			_canvas.sortingLayerName = "Default";
			_canvas.sortingOrder = 4;
		}



		void IZoomable.ZoomOut ()
		{
			ISlideable slideable = this as ISlideable;

			if (!slideable.IsZoomed)
				return;

			RemoveCanvas ();

			slideable.IsZoomed = false;
			OnFocus (slideable);

			MoveTo (_startingAnchor);
			ScaleTo (
				(scalingFinished) => {
					if (scalingFinished) {
					}
				},
				_startingScale
			);
		}



		void RemoveCanvas ()
		{
			Destroy (GetComponent<Canvas> ());
		}



		event Action<ISlideable, SlideDirections> Slide;

		event Action<ISlideable, SlideDirections> ISlideable.Slide {
			add {
				Slide += value;
			}
			remove {
				Slide -= value;
			}
		}

		void OnSlide (ISlideable sender, SlideDirections direction)
		{
			if (Slide != null)
				Slide (sender, direction);
		}



		void ISlideable.PerformSlide (SlideDirections direction)
		{
			IZoomable zoomable = this as IZoomable;

			zoomable.ZoomOut ();
			OnSlide (this as ISlideable, direction);
		}

		

		void Awake ()
		{
			CacheComponents ();
		}



		void CacheComponents ()
		{
			_rectTransform = GetComponent<RectTransform> ();
			_startingAnchor = _rectTransform.anchoredPosition;
			_startingScale = _rectTransform.localScale;
		}



		void MoveTo (Vector2 position, float time = animationSpeed)
		{
			_rectTransform.DOAnchorPos (
				endValue: position,
				duration: time
			);
		}



		void ScaleTo (Vector2 scale, float time = animationSpeed)
		{
			_rectTransform.DOScale (
				endValue: scale,
				duration: time
			);
		}



		void ScaleTo (Action<bool> finished, Vector2 scale, float time = animationSpeed)
		{
			_rectTransform.DOScale (
				endValue: scale,
				duration: time
			).OnComplete (() => {
				finished (true);
			});
		}



		public void OnPointerDown (PointerEventData data)
		{
			IZoomable zoomable = this as IZoomable;
			zoomable.ZoomIn ();
		}
	}

}
public enum SlideDirections
{
	Left,
	Right
}
