﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace DialogUnlockNow
{
	public class UIButtonSlideDirection : MonoBehaviour
	{
		const float fadeDuration = 0.3f;

		[SerializeField]
		SlideDirections direction;
		Button _button;
		Image _image;
		UIUnlockNow _uiUnlockNow;



		public Action<SlideDirections> Clicked;

		void OnClicked (SlideDirections targetDirection)
		{
			if (Clicked != null)
				Clicked (targetDirection);
		}



		void Awake ()
		{
			CacheComponents ();
			SubscribeToEnablingConditions ();
			ConfigButtonToSendDirection ();
		}



		void Start ()
		{
			HideButton ();
		}



		void CacheComponents ()
		{
			_button = GetComponent<Button> ();
			_image = GetComponent<Image> ();
			_uiUnlockNow = GetComponentInParent<UIUnlockNow> ();
		}



		void SubscribeToEnablingConditions ()
		{
			_uiUnlockNow.IsZoomedInImage += ZoomInIsEnabledHandler;
		}



		void ZoomInIsEnabledHandler (bool isEnabled)
		{
			_button.interactable = isEnabled;

			_image.DOFade (
				isEnabled ? 1f : 0f,
				fadeDuration
			);
		}



		void ConfigButtonToSendDirection ()
		{
			_button.onClick.AddListener (() => {
				OnClicked (direction);
			});
		}



		void HideButton ()
		{
			_button.interactable = false;
			_image.color = new Color (1f, 1f, 1f, 0f);
		}



		void OnDestroy ()
		{
			UnsubscribeFromEnablingConditions ();
		}



		void UnsubscribeFromEnablingConditions ()
		{
			_uiUnlockNow.IsZoomedInImage -= ZoomInIsEnabledHandler;
		}
	}
}