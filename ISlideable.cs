﻿using System;

public interface ISlideable : IZoomable
{
	int Index{ get; set; }

	event Action<ISlideable> Focus;

	event Action<ISlideable, SlideDirections> Slide;

	void PerformSlide (SlideDirections direction);
}
