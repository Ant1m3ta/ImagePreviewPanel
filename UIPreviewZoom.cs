﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;

namespace DialogUnlockNow
{
	public class UIPreviewZoom : MonoBehaviour, IPointerDownHandler
	{
		const float fadeTime = 0.5f;
		bool _isActive = false;
		bool _inTransition = false;

		Image _image;

		public event Action CloseZoom;

		void OnCloseZoom ()
		{
			if (CloseZoom != null)
				CloseZoom ();
		}



		void Awake ()
		{
			CacheComponents ();
		}



		void CacheComponents ()
		{
			_image = GetComponent<Image> ();
		}



		public void ZoomHandler (IZoomable sender)
		{
			if (!_isActive && !_inTransition) {
				_isActive = true;
				_inTransition = true;
				_image.raycastTarget = true;
				_image.DOFade (0.5f, fadeTime)
					.OnComplete (() => {
					_inTransition = false;
				});
			}
		}



		public void OnPointerDown (PointerEventData data)
		{
			if (_isActive && !_inTransition)
				StartClosingZoom ();
		}



		void StartClosingZoom ()
		{
			OnCloseZoom ();
			_isActive = false;
			_inTransition = true;
			_image.DOFade (0f, fadeTime)
				.OnComplete (() => {
				_inTransition = false;
				_image.raycastTarget = false;
			});
		}
	}
}