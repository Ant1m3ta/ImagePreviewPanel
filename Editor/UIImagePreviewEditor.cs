﻿using UnityEditor;

namespace DialogUnlockNow
{
	[CustomEditor (typeof(UIImagePreview))]
	public class UIImagePreviewEditor : Editor
	{
		public override void OnInspectorGUI ()
		{
			UIImagePreview image = (UIImagePreview)target;
			ISlideable slide = image as ISlideable;

			slide.Index = EditorGUILayout.IntField ("Index: ", slide.Index);
		}
	}
}
