﻿using System;

public interface IZoomable
{
	bool IsZoomed { get; set; }

	event Action<IZoomable> Zoom;

	void ZoomIn ();

	void ZoomOut ();
}
