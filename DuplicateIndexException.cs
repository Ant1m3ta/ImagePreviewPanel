﻿using System;

namespace DialogUnlockNow
{
	public class DuplicateIndexException : Exception
	{
		public DuplicateIndexException ()
		{
		}

		public DuplicateIndexException (string message) : base (message)
		{
		}
	}
}
