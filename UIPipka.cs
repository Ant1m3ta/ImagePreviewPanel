﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace DialogUnlockNow
{
	public class UIPipka : MonoBehaviour
	{
		[SerializeField]
		int _index = 0;
		[SerializeField]
		Sprite _idleSprite;
		[SerializeField]
		Sprite _selectedSprite;
		Image _image;



		void Awake ()
		{
			CachecComponents ();
		}



		void CachecComponents ()
		{
			_image = GetComponent<Image> ();
		}



		void Start ()
		{
			HidePipka ();
		}



		void HidePipka ()
		{
			_image.color = new Color (1f, 1f, 1f, 0f);
		}



		public void IsZoomEnabledHandler (bool isEnabled)
		{
			_image.DOFade (isEnabled ? 1f : 0f, 0.5f);
		}



		public void FocusHandler (ISlideable sender)
		{
			if (_index == sender.Index)
				_image.sprite = sender.IsZoomed ? _selectedSprite : _idleSprite;
		}
	}
}