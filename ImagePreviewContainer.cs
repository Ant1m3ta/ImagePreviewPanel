﻿using System;
using UnityEngine;
using System.Collections;

namespace DialogUnlockNow
{
	public class ImagePreviewContainer
	{
		ISlideable[] slides;

		int LowestIndex {
			get;
			set;
		}

		int HighestIndex {
			get;
			set;
		}

		public int Length {
			get;
			set;
		}



		public ImagePreviewContainer (ISlideable[] slides)
		{
			this.slides = slides;
			Length = slides.Length;

			SetLowestAndHighestIndexes ();
		}



		void SetLowestAndHighestIndexes ()
		{
			int lowest = int.MaxValue;
			int highest = int.MinValue;

			for (int i = 0; i < slides.Length; i++) {
				int imageIndex = slides [i].Index;


				if (imageIndex < lowest)
					lowest = imageIndex;
				else if (imageIndex > highest)
					highest = imageIndex;
				else if (imageIndex == lowest || imageIndex == highest)
					throw new DuplicateIndexException (imageIndex.ToString ());
				
			}

			LowestIndex = lowest;
			HighestIndex = highest;
		}



		public ISlideable GetSlide (int slideIndex)
		{
			for (int i = 0; i < slides.Length; i++)
				if (slides [i].Index == slideIndex)
					return slides [i];

			throw new IndexOutOfRangeException (slideIndex.ToString ());
		}



		public ISlideable GetSlide (int slideIndex, int offset)
		{
			slideIndex += offset;

			if (slideIndex < LowestIndex)
				slideIndex = HighestIndex;
			else if (slideIndex > HighestIndex)
				slideIndex = LowestIndex;

			for (int i = 0; i < slides.Length; i++)
				if (slides [i].Index == slideIndex)
					return slides [i];

			throw new IndexOutOfRangeException (slideIndex.ToString ());
		}
	}
}