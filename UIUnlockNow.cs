﻿using System;
using System.ComponentModel;
using UnityEngine;
using System.Collections.Generic;

namespace DialogUnlockNow
{
	public class UIUnlockNow : MonoBehaviour
	{
		bool _isZoomedIn = false;

		ImagePreviewContainer _imageContainer;
		UIButtonSlideDirection[] _directionButtons;
		UIPreviewZoom _previewBackground;
		UIPipka[] _pipkas;



		public event Action<bool> IsZoomedInImage;

		void OnIsZoomedInImage (bool isEnabled)
		{
			if (IsZoomedInImage != null)
				IsZoomedInImage (isEnabled);
		}

		bool IsZoomedIn {
			get {
				return _isZoomedIn;
			}
			set {
				_isZoomedIn = value;

				OnIsZoomedInImage (_isZoomedIn);
			}
		}



		void Awake ()
		{
			CacheComponents ();
			SubscribeToImageEvents ();
			SubscribeToPanelEvents ();
		}



		void CacheComponents ()
		{
			_imageContainer = new ImagePreviewContainer (GetComponentsInChildren<UIImagePreview> ());
			_directionButtons = GetComponentsInChildren<UIButtonSlideDirection> ();
			_previewBackground = GetComponentInChildren<UIPreviewZoom> ();
			_pipkas = GetComponentsInChildren<UIPipka> ();
		}



		void SubscribeToImageEvents ()
		{
			for (int i = 0; i < _imageContainer.Length; i++) {
				ISlideable slideable = _imageContainer.GetSlide (i);

				slideable.Zoom += _previewBackground.ZoomHandler;
				slideable.Zoom += OpenZoom;
				slideable.Focus += FocusHandler;
				slideable.Slide += SlideHandler;

				for (int j = 0; j < _pipkas.Length; j++)
					slideable.Focus += _pipkas [j].FocusHandler;
			}
		}



		void SubscribeToPanelEvents ()
		{
			_previewBackground.CloseZoom += CloseZoom;

			for (int i = 0; i < _pipkas.Length; i++)
				IsZoomedInImage += _pipkas [i].IsZoomEnabledHandler;
		}



		void OpenZoom (IZoomable sender)
		{
			IsZoomedIn = true;
		}



		void CloseZoom ()
		{
			IsZoomedIn = false;
		}



		void FocusHandler (ISlideable sender)
		{
			CheckIfZoomableMustBeSubscribedToEvents (sender);
		}



		void SlideHandler (ISlideable sender, SlideDirections direction)
		{
			CheckIfZoomableMustBeSubscribedToEvents (sender);
			ISlideable nextSlide = GetNextSlide (sender, direction);
			nextSlide.ZoomIn ();
		}



		ISlideable GetNextSlide (ISlideable currentSlide, SlideDirections slideDirection)
		{
			switch (slideDirection) {
			case SlideDirections.Left:
				return _imageContainer.GetSlide (currentSlide.Index, offset: -1);
			case SlideDirections.Right:
				return _imageContainer.GetSlide (currentSlide.Index, offset: 1);
			}

			throw new InvalidEnumArgumentException (slideDirection.ToString ());
		}



		void CheckIfZoomableMustBeSubscribedToEvents (ISlideable target)
		{
			if (target.IsZoomed)
				SubscribeSlideToEvents (target);
			else
				UnsubscribeSlideToEvents (target);
		}



		void SubscribeSlideToEvents (ISlideable targetToSlide)
		{
			_previewBackground.CloseZoom += targetToSlide.ZoomOut;

			for (int i = 0; i < _directionButtons.Length; i++)
				_directionButtons [i].Clicked += targetToSlide.PerformSlide;
		}



		void UnsubscribeSlideToEvents (ISlideable targetToSlide)
		{
			_previewBackground.CloseZoom -= targetToSlide.ZoomOut;

			for (int i = 0; i < _directionButtons.Length; i++)
				_directionButtons [i].Clicked -= targetToSlide.PerformSlide;
		}
	}
}